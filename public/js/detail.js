//avalon
avalon.ready(function() {
    $.get("/api/book"+location.search,function(data){
        avalon.define("form-box", function(vm) {
            vm.book_data = data;
        });
        avalon.scan();
    });

    $(".main-body .btn-primary").click(function(){
        $.post("/api/user_book"+location.search,{},function(data){
            if(data){
                alert("订购成功");
            }
        });
    });
});