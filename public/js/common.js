function checkLogin(){
    var user = getCookie("username");
    console.log(user);
    if(user == "root"){
        if(location.pathname!="/addBook.html"){
            location.assign("/addBook.html");
        }
        
    }
    if(user&&user.length){
        $(".login").hide();
        $(".userdata .username").html(user).attr("href");
        $(".userdata").removeClass("hide");
    }
}

function getCookie(cookieName){
    var cookieObj={},
    cookieSplit=[],
    cookieArr=document.cookie.split(";");
    for(var i=0,len=cookieArr.length;i<len;i++){
        if(cookieArr[i]) {
            cookieSplit=cookieArr[i].split("=");
            cookieObj[cookieSplit[0].replace(/(^\s*)|(\s*$)/g,'')]=cookieSplit[1].replace(/(^\s*)|(\s*$)/g,'');
        }
    }
        
    return cookieObj[cookieName];
}
avalon.ready(function() {

    checkLogin();

    $("#signin .btn-primary").click(function(){
        var data = {
            "username": $("#signin .username").val(),
            "password": $("#signin .password").val()
        }
        $.post("/api/user",data,function(data){
            console.log(data);
            $("#signin").modal('hide');
        });
    });

    $("#login .btn-primary").click(function(){
        var data = {
            "username": $("#login .username").val(),
            "password": $("#login .password").val()
        }
        $.get("/api/user",data,function(data){
            console.log(data);
            if(data){
                checkLogin();
                $("#login").modal('hide');
            }else{
                alert("用户账号密码错误");
            }
            
        });
    });

    $(".logout").click(function(){
        $.get("/api/logout",function(data){
            if(data){
                $(".userdata").hide();
                $(".login").removeClass("hide");
            }else{
            }
            
        });
    });

    $(".sreach button").click(function(){
        location.assign("./list.html?q="+$(".sreach input").val());
    });


    $.get("/api/user_book",function(data){
        $(".car b").html(data.length);
    });

});
