//avalon
avalon.ready(function() {
    var data = [
        {
            "href": "/detail.html?ISBN=9787115352460",
            "src": "./imgs/1.jpg",
            "name": "NodeJs实战",
            "value": 65
        },
        {
            "href": "/detail.html?ISBN=9787111376613",
            "src": "./imgs/2.jpg",
            "name": "JavaScript权威指南",
            "value": 80
        },
        {
            "href": "/detail.html?ISBN=9787115373984",
            "src": "./imgs/3.jpg",
            "name": "Go并行编程实践",
            "value": 45
        },
        {
            "href": "/detail.html?ISBN=9787121177408",
            "src": "./imgs/4.jpg",
            "name": "JavaScript语言精粹",
            "value": 23
        },
        {
            "href": "/detail.html?ISBN=9787115333209",
            "src": "./imgs/5.jpg",
            "name": "自制编程语言",
            "value": 90
        },
        {
            "href": "/detail.html?ISBN=9787115352460",
            "src": "./imgs/1.jpg",
            "name": "NodeJs实战",
            "value": 65
        },
    ];


    //
    avalon.define("form-box", function(vm) {
        vm.book_data = data;
    });
    avalon.scan();

});