var queryString = location.search.slice(1).split("=")[1];

avalon.ready(queryString?a:b);

function a(){
    var index;
    $(".container .username").html(queryString);
    $(".container .username").attr("href","./user.html?username="+queryString);
    $(".toIndex a").attr("href","./index.html?username="+queryString);
    var storage = window.localStorage;

    var ticketDatas = storage.getItem(queryString)?JSON.parse(storage.getItem(queryString)):{};
    var keys = Object.keys(ticketDatas);
    var data = [];
    for(ticketData in ticketDatas){
        data.push(JSON.parse(storage.getItem("data"+ticketData)));
    }



    avalon.define("form-box", function(vm) {
        vm.air_data = data;
    });
    avalon.scan();

    $(".nav-profile").click(function(){
        $(".ticket_detail").addClass("hide");
        $(".nav-ticketProfile").removeClass("active");
        $(".nav-profile").addClass("active");
        $(".profile").removeClass("hide");
    });

    $(".nav-ticketProfile").click(function(){
        $(".profile").addClass("hide");
        $(".nav-profile").removeClass("active");
        $(".nav-ticketProfile").addClass("active");
        $(".ticket_detail").removeClass("hide");
    });

    $("#myModal .btn-primary").click(function(){
        $("tr")[index+1].innerHTML = "";
        $("#myModal").modal('hide');
    });

    $(".mybutton").click(function(){
        index = $(this).parent().parent().index();
    })
}


function b(){
    alert("请先登录");
    location.assign("./index.html");
}