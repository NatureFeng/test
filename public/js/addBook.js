avalon.ready(function(){

    $.get("/api/outofstock",function(data){
        avalon.define("form-box", function(vm) {
            vm.book_data = data;
        });
        avalon.scan();
    })

    $("form .btn-primary").click(function(e){
        e.preventDefault();
        var element = $("form input");
        var data = {
            "bookname": element[1].value,
            "ISBN": element[0].value,
            "Publisher": element[3].value,
            "Author": element[2].value,
            "value": element[4].value,
            "number": element[5].value,
        };
        var validata = true;
        var warnString = [];
        for(dataDetal in data){
            if(!data[dataDetal]){
                validata = false;
                warnString.push(dataDetal);
            }
        }

        if(!validata){
            alert("您的"+warnString.join(",")+"有误");
            return ;
        }
        $.post("/api/book",data,function(status){
            console.log("status:",status);
            if(status){
                console.log("success!");
                alert("添加成功");
            }
        });
    });
});