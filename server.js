var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql = require('mysql');
var cookieParser = require('cookie-parser');
var router = express.Router();

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());



// load the cookie parsing middleware
app.use(cookieParser());



var connection = mysql.createConnection({
  host     : 'localhost',
  //user     : 'node',
  user     : 'root',
  //password : 'L2XbFSDFLbK40Z5j'
  password : '19931220'
});

connection.connect();

connection.query("use node");


router.route('/book').get(function(req, res) {
    var queryString = 'select * from books where ISBN="'+req.query.ISBN+'"';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.send(rows);
    });
}).post(function(req, res) {
    var queryString = 'insert into books values("'+req.body.bookname+'","'+req.body.ISBN+'","'+req.body.Publisher+'","'+req.body.Author+'",'+req.body.value+','+req.body.number+')';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.send("1");
    });
}).put(function(req, res) {
    var queryString = 'update books set number=number-1 where number > 0 and ISBN ="'+req.query.ISBN+'"';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.send("1");
    });
});

router.route('/user').get(function(req, res) {
    if(req.query.username == "root"){
        res.cookie('username', req.query.username, { maxAge: 900000, httpOnly: false })
        res.send("1");
        return;
    }
    var queryString = 'select * from user where username="'+req.query.username+'" and password="'+req.query.password+'"';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.cookie('username', req.query.username, { maxAge: 900000, httpOnly: false })
        res.send(rows[0]);
    });
}).post(function(req, res) {
    var queryString = 'insert into user values("'+req.body.username+'","'+req.body.password+'")';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.send("1");
    });
});

router.route('/logout').get(function(req, res) {
    res.cookie('username', '', { maxAge: 900000, httpOnly: false });
    res.send("1");
});

router.route('/sreach').get(function(req, res) {
    var keywords = req.query.q.split(" ");
    for(index in keywords){
        keywords[index] = "bookname like '%"+keywords[index]+"%'";
    }
    var queryString = 'select * from books where '+keywords.join(" and ");
    console.log(queryString)
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.send(rows);
    });
});

router.route('/user_book').get(function(req, res) {
    var queryString = 'select * from Basket where status!=1 and username="'+req.cookies.username+'"';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.send(rows);
    });
}).post(function(req, res) {
    var queryString = 'select * from books where ISBN="'+req.query.ISBN+'"';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        console.log(queryString);
        if(rows.length!=0){
            queryString = 'insert into Basket values("'+req.cookies.username+'","'+rows[0].bookname+'","'+rows[0].ISBN+'","'+rows[0].Publisher+'","'+rows[0].Author+'",'+rows[0].value+',1,0)';
            connection.query(queryString, function(err, rows, fields) {
                if (err) throw err;
                queryString = 'update books set number=number-1 where number > 0 and ISBN ="'+req.query.ISBN+'"';
                connection.query(queryString, function(err, rows, fields) {
                    if (err) throw err;
                    res.send("1");
                });
            });
        }
    });
});

router.route('/outofstock').get(function(req, res) {
    var queryString = 'select * from outofstock';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.send(rows);
    });
}).post(function(req, res) {
    var queryString = 'insert into outofstock values("'+req.body.bookname+'","'+req.body.ISBN+'","'+req.body.Publisher+'","'+req.body.Author+'",'+req.body.value+','+req.body.number+')';
    connection.query(queryString, function(err, rows, fields) {
        if (err) throw err;
        res.send("1");
    });
})



app.use('/api', router);


app.use(express.static(__dirname + '/public'));


var server = app.listen(8889, function() {
    console.log('Listening on port %d', server.address().port);
});


